<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <style>
    </style>
</head>
<body>
    <center><form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
        Autor: <input type="text" name="autor">
        <input type="submit" name="enviar">
    </form></center><br>
    <div id="here"></div>
    <script src="//d3js.org/d3.v3.min.js" charset="utf-8"></script>


<?php

$link = mysqli_connect("localhost", "root", "root", "Visualizacao");
if (!$link) {
    echo "Erro ao conectar com o banco.";
}

mysqli_set_charset($link, 'utf8');

if (isset($_POST['enviar'])){
$autor = $_POST['autor'];

$sql = "select avg(t.`Book-Rating`) as avg, v.`Image-URL-M` as url from `BX-Book-Ratings` t, `BX-Books` v
            where t.`ISBN` = v.`ISBN` and t.`Book-Rating` != 0 and v.`Book-Author` like '%$autor%'
            group by t.`ISBN`;";

$result = mysqli_query($link, $sql);

for ($i = 0; $i < mysqli_num_rows($result); $i++) {
    $data[] = mysqli_fetch_assoc($result);
}

$data = json_encode($data);
echo '
    <script>
        var width = 700,
            height = 2000;
        
        var canvas =  d3.select("#here")
                        .attr("width", width)
                        .attr("height", height);
    canvas.selectAll("img")
    .data(' . $data . ')
    .enter()
    .append("img")
    .attr("src", function(data) { return data.url; });
    </script>';
}
?>
</body>
</html>
